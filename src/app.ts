import express from "express";
import bodyParser from 'body-parser';
import database_handler from './postgres/DatabaseHandler';
const database = new database_handler();
const hpp = require('hpp');
const helmet = require('helmet');
const contentLength = require('express-content-length-validator');
const cors = require("cors");
const app = express();

//**  Security Middleware */
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(hpp());
app.use(cors());
app.use(helmet());
app.use(helmet.hidePoweredBy({setTo: 'Vodka'}));
app.use(contentLength.validateMax({max: 9999, status: 400, message: "I see how it is. watch?v=ewRjZoRtu0Y"}));

//Routes
app.use('/users/v1', require('./routes/users')(database));

app.get( "/", ( req, res ) => {
    res.json('Hello');
});

app.listen(3000, () => {
    //database.getUserDatabase().createUser('liamgibs@gmail.com', 'SDFC', '0422010995');
    //database.getCompanyDatabase().createCompany('Testing Company');
    console.log(`Started Authentication Microservice.`);
});