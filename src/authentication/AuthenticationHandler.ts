import express from "express";
import DatabaseHandler from "../postgres/DatabaseHandler";
import jwt from "jsonwebtoken";
import fs from "fs";
import { resolve } from "path";

const privateAccessKey = fs.readFileSync('./keys/privateAccess.pem');
const publicAccessKey = fs.readFileSync('./keys/publicAccess.pem');
const privateRefreshKey = fs.readFileSync('./keys/privateRefresh.pem');
const publicRefreshKey = fs.readFileSync('./keys/publicRefresh.pem');

export default class Authentication_Handler {

    private database:DatabaseHandler;
    constructor(database:DatabaseHandler) {
        this.database = database;
    }


    /**
     * Will produce access & refresh tokens
     * @param usr_id The user instance ID
     */
    public generateAccessRefreshTokens(usr_id: any):any {
        return new Promise(async resolve => {
            //Generate Access Token & Auth Token
            const accessToken = jwt.sign({usr_id: usr_id}, privateAccessKey, { algorithm: 'RS256', expiresIn: '15m'});
            //Gemerate UUID
            let uuid = this.uuidv4();
            const refreshToken = jwt.sign({usr_id: usr_id, isx: uuid}, privateRefreshKey, { algorithm: 'RS256', expiresIn: '7d'});
            await this.database.getUserDatabase().insertRefreshToken(usr_id, refreshToken, uuid);
            return resolve([accessToken, refreshToken]);
        });
    }

    /**
     * Will validate the refresh token
     * @param refreshToken The refresh token
     */
    public decodeRefreshToken(refreshToken: any) {
        return new Promise<any|boolean>(async resolve => {
            return jwt.verify(refreshToken, publicRefreshKey, (err:any, decoded:any) => {
                if(err) return resolve(false);
                return resolve(decoded);
            });
        });
    }

    /**
     * Used to regenerate tokens when refreshing
     * @param usr_id User ID
     * @param uuid Refresh Token UUID
     */
    public regenerateTokens(usr_id: any, uuid: any): any {
        return new Promise(async resolve => {
            //Generate Access Token & Auth Token
            const accessToken = jwt.sign({usr_id: usr_id}, privateAccessKey, { algorithm: 'RS256', expiresIn: '15m'});
            //Gemerate UUID
            const refreshToken = jwt.sign({usr_id: usr_id, isx: uuid}, privateRefreshKey, { algorithm: 'RS256', expiresIn: '7d'});
            await this.database.getUserDatabase().updateRefreshToken(uuid, refreshToken);
            return resolve([accessToken, refreshToken]);
        });
    }

    /**
     * Will validate based on route and accessToken
     * @param accessToken The generated access token
     */
    public authenticationMiddleware(req:any, res:any, next:any):Promise<any> {
        return new Promise<boolean>(async resolve => {
            let token = req.headers['x-access-token'] || req.headers['authorization'];
            if (token) {
                if (token.startsWith('Bearer ')) {
                    // Remove Bearer from string
                    token = token.slice(7, token.length);
                }
                jwt.verify(token, publicAccessKey, (err:any, decoded:any) => {
                  if (err) {
                    return res.status(401).json({
                      status: 'TOKEN_INVALID',
                      message: 'Access token is expired or not valid'
                    });
                  } else {
                    //Access Token is valid.
                    req.decoded = decoded;
                    next();
                  }
                });
              } else {
                return res.status(401).json({
                  status: 'MISSING_TOKEN',
                  message: 'Access token is not provided'
                });
              }
        });
    }


    private uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
    };
}

