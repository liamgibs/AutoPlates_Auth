import express from "express";
import DatabaseHandler from "../postgres/DatabaseHandler";
import jwt from "jsonwebtoken";
import fs from "fs";
let router = express.Router();
import AuthenticationHandler from "../authentication/AuthenticationHandler";

module.exports = (database:DatabaseHandler) => {

    const AuthHandler = new AuthenticationHandler(database);

    router.post('/login', async (req:any, res:any) => {
        const email = req.body.email;
        const password = req.body.password;
        if(!email || !password) return res.status(400).json({error: 'MISSING_DATA', message: 'Fields that are required are missing.'});
        console.time('A');
        let isAuthed = await database.getUserDatabase().loginUser(email, password);
        console.timeEnd('A');
        if(isAuthed[0] == null) return res.status(400).json({error: 'USER_NOT_VALIDATED', message: 'Please verify your account through the email provided.'});
        if(!isAuthed[0]) return res.status(400).json({error: 'INVALID_INFORMATION', message: 'Either the email or password provided is invalid.'});
        let tokens = await AuthHandler.generateAccessRefreshTokens(isAuthed[1]);
        //Store refreshToken in Database
        return res.status(200).json({status: 'Authenticated', access_token : tokens[0], refresh_token: tokens[1]});
    });

    router.post('/refresh', async (req:any, res:any) => {
        let refreshToken = req.headers['x-refresh-token'] || req.headers['refresh'];
        if(!refreshToken) {
            return res.status(401).json({
                status: 'MISSING_TOKEN',
                message: 'Refresh token is invalid'
            });
        } else {
            let decoded = await AuthHandler.decodeRefreshToken(refreshToken);
            let verifyRefreshToken = await database.getUserDatabase().verifyRefreshToken(decoded.isx, decoded.usr_id, refreshToken);
            if(!verifyRefreshToken) {
                return res.status(401).json({
                    status: 'TOKEN_INVALID',
                    message: 'Refresh token is expired or not valid'
                  });
            } else {
                let newTokens = await AuthHandler.regenerateTokens(decoded.usr_id, decoded.isx);
                await database.getUserDatabase().updateRefreshToken(decoded.isx, newTokens[1]);
                return res.status(200).json({status: 'Authenticated', access_token : newTokens[0], refresh_token: newTokens[1]});
            }
        }

    });

    router.post('/secured', AuthHandler.authenticationMiddleware, async (req:any, res:any) => {
        return res.status(200).json('OK');
    })

    return router;

}