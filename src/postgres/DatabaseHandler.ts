import { Pool } from 'pg';
const settings = require('../config/settings.json');
const pool = new Pool({
    user: settings.DB_USER,
    host: settings.DB_HOST,
    database: settings.DB_DATABASE,
    password: settings.DB_PASS,
    port: settings.DB_PORT,
});

import userDatabase from './User_Database';
const User_Database = new userDatabase(pool);

export default class DatabaseHandler {
    
    /**
     * Returns the User Database
     * @returns {userDatabase} The current user database instance
     */
    public getUserDatabase = ():userDatabase => {
        return User_Database;
    }

}