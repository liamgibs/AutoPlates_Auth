const bcrypt = require('bcryptjs');
const _crypto = require("crypto");
import {Pool} from 'pg';
import { link } from 'fs';


export default class User_Database {
    private pool : Pool;

    constructor(pool : Pool) {
        this.pool = pool;
    }


    //      -=- User functions -=-

    /**
     * Create user with email and password. The password is encrypted.
     * @param {String} email The email for the associated account.
     * @param {String} password The password for the account.
     * @returns {Promise} Returns a promise which resolves to false if a error occured or to a user_id.
     */
    public createUser = async (email:string, password:string, mobile:string) => {
        var _this = this;
        return new Promise(async function(resolve, reject) {
            bcrypt.hash(password, 10).then(async (hash:string) => {
            const client = await _this.pool.connect()
            try {
                await client.query('BEGIN')
                const { rows } = await client.query("INSERT INTO users(email, password, verifykey, verifypin, created_on, last_login, mobile, user_id) VALUES($1, $2, $3, $4, now(), now(), $5, $6) RETURNING user_id", [email, hash, _crypto.randomBytes(20).toString("hex"), Math.floor(1000 + Math.random() * 9000), mobile, _this.uuidv4()]);
                await client.query('COMMIT');
                resolve(rows[0].user_id);
            } catch (e) {
                console.log(e);
                await client.query('ROLLBACK')
                resolve(false);
            } finally {
                client.release();
            }
            });
        });
    }

    public deleteUser = async (email:string) => {
        var _this = this;
        return new Promise(async function(resolve, reject) {
            try {
                const res = await _this.pool.query("DELETE from users WHERE email=$1", [email]);
                resolve((res.rowCount == 0) ? false : true);
            } catch (e) {
                resolve(false);
            }
            
        });
    }

    /**
     * Used to login the user with a give email or password.
     * @param {String} email The associated accounts email address.
     * @param {String} password The associated accounts password.
     * @returns {Promise} ifLoggedIn
     */
    public loginUser = async (email:string, password:string):Promise<Array<Boolean|String>> => {
        var _this = this;
        return new Promise<Array<Boolean|String>>(async function(resolve, reject) {
            const client = await _this.pool.connect();
            try {
                await client.query('BEGIN');
                const { rows } = await client.query("SELECT user_id, password, verified, verifykey, verifypin from users where email=$1", [email]);
                if(rows[0] === undefined || rows.length == 0 || rows == null){
                    await client.query('ROLLBACK');
                    resolve([false]);
                }else{
                    bcrypt.compare(password, rows[0].password).then(async (isValidated:boolean) => {
                        if (isValidated) {
                            if(rows[0].verified === 'N') return resolve([null]);
                            return resolve([true, rows[0].user_id]);
                        }else{
                            await client.query('ROLLBACK');
                            resolve([false]);
                        }
                    })
                }
            } catch (e) {
            await client.query('ROLLBACK')
            return resolve([false]);
            } finally {
            client.release();
            }
        }).catch();
    }

    /**
     * Verify a user account, give then verifykey and pin.
     * @param {String} verifykey The given verifiykey for the user.
     * @param {String} pin The pin to verify the user. 
     * @returns {Boolean} Returns a async boolean, true or false if complete.
     */
    public userVerification = async (verifykey:string, pin:string) => {
        try {
            const res = await this.pool.query("UPDATE users SET verified='Y' where verifykey=$1 AND verifypin=$2", [verifykey, pin]);
            return (res.rowCount == 0) ? false : true;
        } catch(e) {
            return false;
        }
    }

    /**
     * Check if an email exists, if so return true or false.
     * @param {String} email The email to search for
     * @returns {Promise} Returns a promise that resolves
     */
    public doesEmailExist = async (email:string) => {
        try {
            const { rows } = await this.pool.query('SELECT user_id from users where email=$1', [email]);
            if (rows[0] === undefined || rows.length == 0 || rows == null) {
                return false;
            } else {
                return true;
            }
        } catch(e) {
            return false;
        }
    }

    /**
     * Will insert a hashed refresh token
     */
    public insertRefreshToken = async (usr_id: any, refresh_token: string, uuid: string) => {
        var _this = this;
        return new Promise(async function(resolve, reject) {
            bcrypt.hash(refresh_token, 10).then(async (hash:string) => {
                const client = await _this.pool.connect()
                try {
                    await client.query('BEGIN')
                    await client.query("INSERT INTO public.refresh_tokens(uuid, usr_id, refresh_token, cmpy, created) VALUES ($1, $2, $3, now());", [uuid, usr_id, hash]);
                    await client.query('COMMIT');
                    resolve(true);
                } catch (e) {
                    console.log(e);
                    await client.query('ROLLBACK')
                    resolve(false);
                } finally {
                    client.release();
                }
            });
        });
    }

    /**
     * Will update an existing Refresh Token.
     */
    public updateRefreshToken = async (uuid:string, refresh_token:string) => {
        try {
            bcrypt.hash(refresh_token, 10).then(async (hash:string) => {
                const res = await this.pool.query("UPDATE refresh_tokens SET refresh_token=$1, refreshed = now() where uuid=$2", [hash, uuid]);
                return true;
            });
        } catch(e) {
            return false;
        }
    }

    public verifyRefreshToken = async (uuid: string, usr_id: string, refresh_token : string):Promise<boolean> => {
        var _this = this;
        return new Promise<boolean>(async function(resolve, reject) {
            try {
                const { rows } = await _this.pool.query("SELECT refresh_token from refresh_tokens where uuid=$1 and usr_id = $2", [uuid, usr_id]);
                if(rows[0] === undefined || rows.length == 0 || rows == null){
                    resolve(false);
                }else{
                    bcrypt.compare(refresh_token, rows[0].refresh_token).then(async (isValidated:boolean) => {
                        if (isValidated) {
                            return resolve(true);
                        } else {
                            resolve(false);
                        }
                    })
                }
            } catch (e) {
                console.log(e, 'B');
                return resolve(false);
            }
        }).catch();
    }

    private uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
    }
}